import { Component, OnInit,Injectable } from '@angular/core';
import{Router}from'@angular/router';
import {AngularFire, FirebaseListObservable} from 'angularfire2';
import* as firebase from'firebase';

@Injectable()
@Component({
  selector: 'app-updateaccounts',
  templateUrl: './updateaccounts.component.html',
  styleUrls: ['./updateaccounts.component.css']
})
export class UpdateaccountsComponent implements OnInit {
public accountList: Array<any>;
public obalanceList:Array<any>;

 public userProfile:firebase.database.Reference;
 public obalance:firebase.database.Reference;
  constructor() {


    firebase.auth().onAuthStateChanged( user => {
      if (user) {
        this.userProfile = firebase.database().ref(`userProfile/${user.uid}`); 
        console.log(this.userProfile);
    

        this.getAccountList().on('value', snapshot => {
      this.accountList = [];
      snapshot.forEach( snap => {
        this.accountList.push({
          id: snap.key,
          name: snap.val().name,
          opbalance:snap.val().opbalance
        });
          
        return false
      });
    });
      }
    //this.accountList.push(this.userProfile);
    });   
  }
  
     
  
 
 getAccountList(): firebase.database.Reference {
 console.log(this.userProfile);
    return this.userProfile;
   
  }
ngOnInit()
{

}


  
}
