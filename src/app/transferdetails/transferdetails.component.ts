import { Component, OnInit,Injectable } from '@angular/core';
import { moveIn,fallIn,moveInLeft } from '../router.animations';

import { AngularFire, FirebaseListObservable } from 'angularfire2';
import* as firebase from'firebase';

@Injectable()
@Component({
  selector: 'app-transferdetails',
  templateUrl: './transferdetails.component.html',
  styleUrls: ['./transferdetails.component.css'],
  animations: [moveIn(), fallIn(), moveInLeft()],
  host: {'[@moveIn]': ''}
})
export class TransferdetailsComponent implements OnInit {
 public accountList: Array<any>;


public date:Date;
   public details:string;
   public amount:number;
 public userProfile:firebase.database.Reference;
  constructor() {

this.date=new Date();
    firebase.auth().onAuthStateChanged( user => {
      if (user) {
        this.userProfile = firebase.database().ref(`userProfile/${user.uid}`); 
        console.log(this.userProfile);

        this.getAccountList().on('value', snapshot => {
      this.accountList = [];
      snapshot.forEach( snap => {
        this.accountList.push({
          id: snap.key,
          name: snap.val().name
        });
          
        return false
      });
    });
      }
    //this.accountList.push(this.userProfile);
  });   
  }

  ngOnInit() {
  }


  subtransfer(date:Date,to:string,from:string,details:string,amount:number):firebase.Promise<any>
{
   console.log(date);
   console.log(details);
   console.log(amount);
   console.log(to);
  return this.userProfile.child(from).child('Journal').child(date.toString()).set({
    name:from,
      date:new Date(date).toString(),
    num:'NEFT',
    details: 'Transfer',
    withdrawal:amount,
    deposit:'0', 
    balance:0-amount
    
  });
 
}

addtransfer(date:Date,to:string,from:string,details:string,amount:number):firebase.Promise<any>
{
   console.log(date);
   console.log(details);
   console.log(amount);
   console.log(to);
  return this.userProfile.child(to).child('Journal').child(date.toString()).set({
    name:to,
      date:new Date(date).toString(),
    num:'NEFT',
    details: 'Transfer',
    withdrawal:'0',
    deposit:amount, 
    balance:amount
    
  });
 
}
transfer(date:Date,to:string,from:string,details:string,amount:number)
  {
  
    this.addtransfer(date,to,from,details,amount).then(newaccount=>
    {
      //this.navCtrl.pop();
       console.log("transfer="+date,details,amount,to,from);
    //  this._presentToast("Deposits added successfully") ;
       },error=>
    {
      console.log(error);
    
    })
     this.subtransfer(date,to,from,details,amount).then(newaccount=>
    {
      //this.navCtrl.pop();
       console.log("transfer="+date,details,amount,to,from);
    //  this._presentToast("Deposits added successfully") ;
       },error=>
    {
      console.log(error);
    
    })
 
    alert("details added");
   
    
  }


getAccountList(): firebase.database.Reference {
 console.log(this.userProfile);
    return this.userProfile;
   
  }
}
