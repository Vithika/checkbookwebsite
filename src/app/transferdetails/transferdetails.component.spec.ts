import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransferdetailsComponent } from './transferdetails.component';

describe('TransferdetailsComponent', () => {
  let component: TransferdetailsComponent;
  let fixture: ComponentFixture<TransferdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransferdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
