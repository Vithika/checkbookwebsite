import { Component, OnInit ,Injectable} from '@angular/core';
import { Router } from '@angular/router';
import { moveIn, fallIn } from '../router.animations';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import {database} from'firebase';
import*as firebase from'firebase';

@Injectable()
@Component({
  selector: 'app-addentry',
  templateUrl: './addentry.component.html',
  styleUrls: ['./addentry.component.css'],
  animations: [moveIn(), fallIn()],
  host: {'[@moveIn]': ''}
})
export class AddentryComponent implements OnInit {

  public accountList: Array<any>;
public date: Date;
  public num: string;
  public details:string;
  public withdrawal:number;
  public deposit:number;
public accountname:string;
 public userProfile:firebase.database.Reference;
  constructor() {

this.date=new Date();
    firebase.auth().onAuthStateChanged( user => {
      if (user) {
        this.userProfile = firebase.database().ref(`userProfile/${user.uid}`); 
        console.log(this.userProfile);

        this.getAccountList().on('value', snapshot => {
      this.accountList = [];
      snapshot.forEach( snap => {
        this.accountList.push({
          id: snap.key,
          name: snap.val().name
        });
          
        return false
      });
    });
      }
    //this.accountList.push(this.userProfile);
    });   
  
  }
  
 onSubmit(formData)
 {
   
 }

 getAccountList(): firebase.database.Reference {
 console.log(this.userProfile);
    return this.userProfile;
   
  }

  addjournal(value:string,date:Date, num:string,details:string,withdrawal:number,deposit:number):firebase.Promise<any>
{
   console.log(date);
   
  return this.userProfile.child(value).child('Journal').child(date.toString()).set({
    name:value,
    date:new Date(date).toString(),
    num:this.num,
    details: this.details,
    withdrawal:this.withdrawal,
    deposit:this.deposit, 
    balance:this.deposit-this.withdrawal  
   
  });
}
  ngOnInit() {
  }
getjournal(value:string,date:Date,num:string,details:string,withdrawal:number,deposit:number)
{

 this.addjournal(value,date,num,details,withdrawal,deposit).then(newaccount=>
    {
      //this.navCtrl.pop();
       console.log("journal="+date,details,withdrawal,deposit,num);
    //  this._presentToast("Deposits added successfully") ;
       },error=>
    {
      console.log(error);
    
    })
    alert("Entries Added");
   
    
  }
  clear()
  {
    this.num='';
    this.withdrawal=0.00;
    this.deposit=0.00;
    this.details='';
    this.date=new Date();
        

  }

}
