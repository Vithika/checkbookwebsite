import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetdepositComponent } from './budgetdeposit.component';

describe('BudgetdepositComponent', () => {
  let component: BudgetdepositComponent;
  let fixture: ComponentFixture<BudgetdepositComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BudgetdepositComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BudgetdepositComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
