import { Component, OnInit } from '@angular/core';
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';
import { Router } from '@angular/router';
import { moveIn, fallIn, moveInLeft } from '../router.animations';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css'],
  animations: [moveIn(), fallIn(), moveInLeft()],
  host: {'[@moveIn]': ''}
})
export class MembersComponent implements OnInit {
  name: any;
  state: string = '';

  constructor(public af: AngularFire,private router: Router) {
    this.af.auth.subscribe(auth => {
      if(auth)
       {
        this.name = auth;
      }
    });

  }

  logout() {
     this.af.auth.logout();
     this.router.navigateByUrl('/login');
  }
  menu() {
    
     this.router.navigateByUrl('/menu');
  }
addentry()
{
 
this.router.navigateByUrl('/addentry');
}
checkbook()
{
  this.router.navigateByUrl('/checkbook');
}

viewaccounts()
{
  this.router.navigateByUrl('/updateaccounts');
}
  ngOnInit() {
  }
}
