import { Component, OnInit,Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { moveIn, fallIn } from '../router.animations';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import {database} from'firebase';
import*as firebase from'firebase';

@Injectable()
@Component({
  selector: 'app-journal',
  templateUrl: './journal.component.html',
  styleUrls: ['./journal.component.css'],
  animations: [moveIn(), fallIn()],
  host: {'[@moveIn]': ''}
})
export class JournalComponent implements OnInit {

    public   journaldetails: FirebaseListObservable<any[]>;


public date: Date;
  public num: string;
  public details:string;
  public withdrawal:number;
  public deposit:number;
public accountname:string;
public balance:number;

 public accountList: Array<any>;

 public userProfile:firebase.database.Reference;
  constructor() {

this.date=new Date();
    firebase.auth().onAuthStateChanged( user => {
      if (user) {
        this.userProfile = firebase.database().ref(`userProfile/${user.uid}`); 
        console.log(this.userProfile);

        this.getAccountList().on('value', snapshot => {
      this.accountList = [];
      snapshot.forEach( snap => {
        this.accountList.push({
          id: snap.key,
          name: snap.val().name
        });
          
        return false
      });
    });
      }
    //this.accountList.push(this.userProfile);
    });   

  }
  getAccountList(): firebase.database.Reference {
 console.log(this.userProfile);
    return this.userProfile;
   
  }

addjournal(value:string,date:Date, num:string,details:string,withdrawal:number,deposit:number):firebase.Promise<any>
{
   console.log(date);
   let balance:number=0.00;
  return this.userProfile.child(value).child('Journal').child(date.toString()).set({
    name:value,
    date:new Date(date).toString(),
    num:this.num,
    details: this.details,
    withdrawal:this.withdrawal,
    deposit:this.deposit, 
    balance:this.deposit-this.withdrawal
   
  

  })
}
  ngOnInit() {
  }
getjournal(value:string,date:Date,num:string,details:string,withdrawal:number,deposit:number)
{

 this.addjournal(value,date,num,details,withdrawal,deposit).then(newaccount=>
    {
      //this.navCtrl.pop();
       console.log("journal="+date,details,withdrawal,deposit,num);
    //  this._presentToast("Deposits added successfully") ;
       },error=>
    {
      console.log(error);
    
    })
    alert("Journal added");
   
    
  }
}
