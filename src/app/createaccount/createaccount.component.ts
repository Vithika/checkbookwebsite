import { Component, OnInit,Injectable} from '@angular/core';
import { Router } from '@angular/router';
import { moveIn, fallIn } from '../router.animations';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import {database} from'firebase';
import*as firebase from'firebase';
@Injectable()
@Component({
  selector: 'app-createaccount',
  templateUrl: './createaccount.component.html',
  styleUrls: ['./createaccount.component.css'],
   animations: [moveIn(), fallIn()],
  host: {'[@moveIn]': ''}
})
export class CreateaccountComponent implements OnInit {
// public    userProfile: FirebaseListObservable<any[]>;
public userProfile:firebase.database.Reference;

public accountname: string;
  public openingbalance: number;
  public paybudgetperiod:number;


public date: Date;

  constructor( private af: AngularFire,private router: Router) {
 this.date=new Date();
 firebase.auth().onAuthStateChanged( user => {
      if (user) {
        this.userProfile = firebase.database().ref(`userProfile/${user.uid}`); 
        console.log(this.userProfile);
      }
    });
 //this. itemObservable = af.database.list('accounts');
  }

  

  ngOnInit() {
  }
 


getformdata(date:Date,accountame:string,openingbalance:number,paybudgetPeriod:number):firebase.Promise<any>

{
   return this.userProfile.child(this.accountname).set({
     date:new Date(date).toString(),
      name: this.accountname,
      paybudget: this. paybudgetperiod,
      opbalance: this.openingbalance
    });
 }
 getdata(date:Date,accountame:string,openingbalance:number,paybudgetPeriod:number):firebase.Promise<any>

{
   return this.userProfile.child(this.accountname).child('Journal').child(date.toString()).set({
    name:accountame,
    date:new Date(date).toString(),
    num:'',
    details: 'Opening Balance',
    withdrawal:'0',
    deposit:this.openingbalance, 
    balance:this.openingbalance
 })
}


  createaccount(date,accountname :string, payperiodbudget :number, opbalance:number) {

  this.getformdata(date,accountname,payperiodbudget,opbalance).then( newaccount => {
   
     // this.navCtrl.pop();
     // this._presentToast("Successfully created account: "+name +new Date().toLocaleString());
    }, error => {
      console.log(error);
    });
  
this.getdata(date,accountname,payperiodbudget,opbalance).then( newaccount => {
     // this.navCtrl.pop();
     // this._presentToast("Successfully created account: "+name +new Date().toLocaleString());
    }, error => {
      console.log(error);
    });
  }
  
}
  

