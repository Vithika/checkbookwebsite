import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AngularFireModule,FirebaseListObservable } from 'angularfire2';
import { FIREBASE_PROVIDERS, defaultFirebase } from 'angularfire2';




import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { EmailComponent } from './email/email.component';
import { SignupComponent } from './signup/signup.component';
import { MembersComponent } from './members/members.component';
import { AuthGuard } from './auth.service';
import { routes } from './app.routes';
import { MenuComponent } from './menu/menu.component';
import { CheckbookComponent } from './checkbook/checkbook.component';
import { AddentryComponent } from './addentry/addentry.component';
import { CreateaccountComponent } from './createaccount/createaccount.component';
import { TransferdetailsComponent } from './transferdetails/transferdetails.component';
import { UserguideComponent } from './userguide/userguide.component';
import { JournalComponent } from './journal/journal.component';
import { UpdateaccountsComponent } from './updateaccounts/updateaccounts.component';
import { BudgetdepositComponent } from './budgetdeposit/budgetdeposit.component';
import { DeleteComponent } from './delete/delete.component';




// Must export the config
export const firebaseConfig = {
    apiKey: "AIzaSyAwHsu01HVZ7cBJluOcwqXC3Tey3VsAPV4",
    authDomain: "checkbookwebsite-fa7f8.firebaseapp.com",
    databaseURL: "https://checkbookwebsite-fa7f8.firebaseio.com",
    projectId: "checkbookwebsite-fa7f8",
    storageBucket: "checkbookwebsite-fa7f8.appspot.com",
    messagingSenderId: "403729379399"
  };
   
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    EmailComponent,
    SignupComponent,
    MembersComponent,
    MenuComponent,
    CheckbookComponent,
    AddentryComponent,
    CreateaccountComponent,
    TransferdetailsComponent,
    UserguideComponent,
    JournalComponent,
    UpdateaccountsComponent,
    BudgetdepositComponent,
    DeleteComponent,
   
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig),
   
    routes
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
