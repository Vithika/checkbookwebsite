import { Component, OnInit,Injectable } from '@angular/core';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import {database} from'firebase';
import*as firebase from'firebase';


@Injectable()
@Component({
  selector: 'app-checkbook',
  templateUrl: './checkbook.component.html',
  styleUrls: ['./checkbook.component.css']
})
export class CheckbookComponent implements OnInit {


  public accountList: Array<any>;
public datesList:Array<any>;

 public userProfile:firebase.database.Reference;
 public dates:firebase.database.Reference;
 public accountdetails:firebase.database.Reference;
 public totaljournalbalance:number=0;

  constructor() {



    firebase.auth().onAuthStateChanged( user => {
      if (user) {
        
        this.userProfile = firebase.database().ref(`userProfile/${user.uid}`); 
      //  this.dates=firebase.database().ref(`userProfile/${user.uid}`).child(value)
        console.log(this.userProfile);

        this.getAccountList().on('value', snapshot => {
      this.accountList = [];
      snapshot.forEach( snap => {
        this.accountList.push({
          id: snap.key,
          name: snap.val().name,
         
        });
        
        return false
       
      });
       console.log(this.accountList);
    });
      }
    //this.accountList.push(this.userProfile);
  });   
 
 
  }
  getAccountList(): firebase.database.Reference {
 console.log(this.userProfile);
    return this.userProfile;
   
  }
  
  
  getdates():firebase.database.Reference{
    return this.dates;
  }
 
  showjournal(value)
  {

    this.dates=this.userProfile.child(value).child('Journal').child('/');

     console.log(this.dates);
    console.log(value);
    this.totaljournalbalance=0;
 this.getdates().on('value',snapshot=>
 {
   this.datesList=[];
   snapshot.forEach(snap=>
   {
     this.datesList.push({
       date:snap.val().date,
       withdrawal:snap.val().withdrawal,
       deposit:snap.val().deposit,
       num:snap.val().num,
       details:snap.val().details,
       balance:snap.val().balance
      
     });

  
      this.totaljournalbalance+=snap.val().balance;
       console.log(this.totaljournalbalance);
     
      
     return false
       
   });
   
   console.log(this.datesList);

 });
}

 
 
   
  ngOnInit() {
  }
/*clear()
{
 
  this.totaljournalbalance=0;
  this.datesList=null;
 
}*/
}
